// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;

	CountApples = 4;
	ScoreApples = 0;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed);

	AddSnakeElement(3);
	
}

FVector ASnakeBase::ReturnMovementVector()
{
	
	FVector MovementVector;

	//
	MovementVector.X = 0;
	MovementVector.Y = 0;
	MovementVector.Z = 0;
	//

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;

	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;

	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	return MovementVector;
}


// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; ++i)
	{
		
		FTransform NewTransform;

		if (SnakeElements.Num() == 0)
		{
			//FTransform NewTransform = FTransform(GetActorLocation() - NeVector);
			FVector NewVector(500, 500, 0);
			NewTransform = FTransform(NewVector);
		}
		else
		{
			FVector MovementVector = ASnakeBase::ReturnMovementVector();
			NewTransform = FTransform(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation() - MovementVector);
		}

		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;

		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	
}

void ASnakeBase::Move()
{
	
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1 ; i > 0; i--)
	{
		auto CorrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		
		FVector PrevLocation = PrevElement->GetActorLocation();
		CorrentElement->SetActorLocation(PrevLocation);
	}

	FVector MovementVector = ReturnMovementVector();
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOveplap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = (ElemIndex == 0);

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface)
		{
			this->MovementSpeed *= 0.9f;
			float MinSpeed = 0.1f;
			if (this->MovementSpeed < MinSpeed)
			{
				this->MovementSpeed = MinSpeed;
			}

			SetActorTickInterval(this->MovementSpeed);

			InteractableInterface->Interact(this, bIsFirst);
			
		}
		else if (bIsFirst)
		{
			OverlappedElement->SnakeOwner->Destroy();
		}
	}
}


