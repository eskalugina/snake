// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	multiplierX = 1.f;
	multiplierY = 1.f;

	MovementSpeed = 1.f;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed);
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void AFood::AddApples(int32 CountApples)
{
	for (int32 i = 1; i <= CountApples; ++i)
	{
		float SpawnX = FMath::FRandRange(50.f, 950.f);
		float SpawnY = FMath::FRandRange(-150.f, 1150.f);
		FVector NewVector(SpawnX, SpawnY, 10.f);

		FTransform NewTransform = FTransform(NewVector);
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform);

	}
}


void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->Destroy();
			Snake->CountApples -= 1;
			Snake->ScoreApples += 1;

			//
			int32 CountApples = static_cast<int32>(FMath::FRandRange(0.f, 3.f));

			if (Snake->CountApples == 0 && CountApples == 0)
			{
				CountApples = 2;
			}

			AFood::AddApples(CountApples);
			Snake->CountApples += CountApples;
		}
	}
}

void AFood::Move()
{
	FVector FoodLocation = this->GetActorLocation();

	if (FoodLocation.X > 950.f || FoodLocation.X < 50.f)
	{
		multiplierX *= -1.f;
	}

	if (FoodLocation.Y > 1150.f || FoodLocation.Y < -150.f)
	{
		multiplierY *= -1.f;
	}
		
	float SpawnX = 3.f * multiplierX;
	float SpawnY = 2.f * multiplierY;
	
	FVector MovementVector(SpawnX, SpawnY, 0.f);
	this->AddActorWorldOffset(MovementVector);

}


